console.log("Hello World");

let number = 2;
let exponent = 3;
let getCube = (number ** exponent);

console.log(getCube);
console.log( `"The cube of" ${number} "is" ${getCube}`);

const address = {
	houseNumber: "10",
	streetName: "Morion",
	cityAddress: "Baguio"
}

const { houseNumber, streetName, cityAddress } = address;

console.log(houseNumber);
console.log(streetName);
console.log(cityAddress);

function getFullAddress({ houseNumber, streetName, cityAddress }) {
	console.log( `I live at ${houseNumber}, ${streetName}, ${cityAddress }. Welcome!`)
}
getFullAddress(address);


const Tony = {
	height: 70,
	weight: 110,
	trait: "fierce"
}

const { height, weight, trait } = Tony;

console.log(height);
console.log(weight);
console.log(trait);

function getTigerCharacter({ height, weight, trait }) {
	console.log( `I own a tiger that is ${height} cm , ${weight} kgs and that is ${trait}.`)
}
getTigerCharacter(Tony);

let numbers = [3, 6, 9, 12, 15];


numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x +y );
console.log(reduceNumber);

class Dog {
	constructor (age, breed, color)  {

   		this.age = age;
		this.breed = breed;
		this.color = color;
	}
}
 const myDog = new Dog(12, "Alaskan Malamute", "white");
 console.log(myDog);